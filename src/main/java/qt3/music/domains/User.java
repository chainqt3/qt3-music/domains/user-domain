package qt3.music.domains;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serial;
import java.io.Serializable;
import java.security.SecureRandomParameters;

@Document("users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @Serial private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String firstname;
    private String lastname;
    private String gender;
    private String email;
}
